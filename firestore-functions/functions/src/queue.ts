import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

const db = admin.firestore();

interface queueData {
    queue_id: string;
    title?: string;
    capacity?: number;
    category?: "super_market" | "drug_store" | "other";
    address?: string;
    description?: string;
    latitude?: number;
    longitude?: number;
}

export const queue = functions.https.onRequest((request, response) => {
    if (request.get('Content-Type') === 'application/json') {
        const body: queueData = request.body;
        switch (request.method) {
            case 'GET':
                const store = db.collection("Queues").doc(body.queue_id);
                store.get().then(doc => {
                    response.status(200).send(doc.data());
                }).catch(function (error) {
                    response.status(404).send(`Error getting store info: ${String(error)}`);
                });
                break;
            case 'POST':
                db.collection("Queues").add(
                    body
                ).then(function (docRef: any) {
                    response.status(200).send(`Document written with ID: ${String(docRef.id)}`);
                }).catch(function (error: any) {
                    response.status(404).send(`Error adding document: ${String(error)}`);
                });
                break;
            case 'PUT':
                db.collection("Queues").doc(body.queue_id).update(body).then(function (doc) {
                    response.status(200).send(`Document updated with ID: ${String(body.queue_id)}`);
                }).catch(function (error: any) {
                    response.status(404).send(`Error updating document: ${String(error)}`);
                });
                break;
            default:
                response.status(405).send({error: 'Something blew up!'});
                break;
        }
    } else {
        response.status(415)
    }
});