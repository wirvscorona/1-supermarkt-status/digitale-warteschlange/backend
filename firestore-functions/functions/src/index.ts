import * as admin from 'firebase-admin';

admin.initializeApp();

const queue = require('./queue'); // import function script
exports.queue = queue.queue; // export function from imported script