# Backend

## Cloud Functions
### Region
Please make sure that all cloud functions are executed in the `europe-west1` region.

Example code:

    exports.myStorageFunction = functions
        .region('europe-west1')
        .storage
        .object()
        .onFinalize((object) => {
          // ...
        });
    
For further information please refer to:
*  https://firebase.google.com/docs/functions/locations
*  https://firebase.google.com/docs/projects/locations
